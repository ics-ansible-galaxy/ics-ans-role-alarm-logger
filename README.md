# ics-ans-role-alarm-logger

Ansible role to install alarm-logger.

## Role Variables

```yaml
alarm_logger_version: 4.6.0
alarm_logger_dir: /opt/alarm-logger
alarm_logger_topics: []
alarm_logger_date_span_units: M
alarm_logger_date_span_value: "1"
alarm_logger_container_network: alarm-logger
alarm_logger_es_port: "9200"
alarm_logger_es_sniff: false
alarm_logger_es_container_name: elasticsearch
alarm_logger_es_container_ports:
  - 9200:9200
  - 9300:9300
alarm_logger_es_container_image: "{{ alarm_logger_es_container_image_name }}:{{ alarm_logger_es_container_image_tag }}"
alarm_logger_es_container_image_name: elasticsearch
alarm_logger_es_container_image_tag: 6.8.5
alarm_logger_es_container_env: {}
alarm_logger_es_data: alarm-logger-data
alarm_logger_kafka_host: localhost
alarm_logger_kafka_port: "9092"
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-alarm-logger
```

## License

BSD 2-clause
