import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('molecule_group')


def test_service(host):
    alarm_server = host.service('alarm-logger')
    assert alarm_server.is_running
